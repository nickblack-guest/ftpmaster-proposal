FTP-Masters Review Delays
=========================

This is a proposal to address concerns made by members of the Debian community
relating to the FTP-Masters team.

Disclaimer: This document exists solely to support resolving the stated
problems. It is **NOT** meant to accuse any person or group of any wrong-doings,
misdeeds, or otherwise.

Concerns Raised
---------------

Summary of the most common concerns:

- The team operates privately.
- Reviews (rejections) are inconsistent.
- Reviews can take an extremely long time.
- The team creates wasteful busy work.

Problems Identified
-------------------

The following describes issues that team members have identified which may
contribute to some or all of the concerns raised.

Stateful Workspace
++++++++++++++++++

One concern raised with the existing reviews is the time that needs to be set
aside in order to effectively perform reviews.

0. It takes time and effort to properly copy/paste configurations and figure out
   how to work with the tools. This is non-trivial for new Trainees.
1. It takes time to open terminal windows, arrange them, open connections,
   and find a package to review. This is trivial, but time consuming.
2. Any large or complex/complicated package requires extra time to be set aside
   or careful (personal) note keeping in order to pick up where one left off.
   This reduces the chances that such a package will be reviewed quickly.

Note for #2 - After a new Trainee has become comfortable with one or two small
packages, they are encouraged to focus on the difficult packages. This provides
the most immediate/useful support to those approving reviews.

Past Review Info
++++++++++++++++

Another concern raised by existing Trainees is the inability to access previous
reviews on the package.

The inability to re-read previous comments increases the likelihood that
previously-identified problems will be missed when a package is re-uploaded.

This is also problematic for Trainees who receive follow-up after a package has
been processed. Most often, Traniees end up looking at the upstream source on a
service such as Github for the source and attempt to rely on the issue reported
about said review to remember what comment was provided.

Cumbersome Tools
++++++++++++++++

As previously indicated, the current tools are quite complex; they require a
non-negligible amount of time to configure and get acquainted with. This makes
it more difficult for new Trainees to pay attention to the content of reviews.

The complexity of these tools and the way they're scripted to integrate with
each other make debugging/troubleshooting a non-trivial task. Most often, the
best solution is to delete configuration files and start over.

Lack of Consistent Review / Transparency
++++++++++++++++++++++++++++++++++++++++

Occasionally, a package maintainer will make a concern about a rejection
because they weren't previously given a rejection for making the same mistake or
because another FTP-Masters team member has a different opinion.

Note: The REJECT-FAQ is not an exhaustive list of reasons to reject.

The lack of consistent review contributes to an appearance of opaqueness. It
also contributes to the frustration of new Trainees who need to wait until they
make a mistake, see a mistake made, or observe the correct note before they're
made aware of the non-/required check.

This is extremely discouraging to Trainees who would prefer work on a small
number of packages and wait for critical review so they can avoid making the
same mistake repeatedly on additional packages.

Some Debian Developers have noted that the FTP-Masters mailing list being
private and the lack of a complete and comprehensive list of reasons for a
reject are major contributors to the observed lack of transparency.

Small Team
++++++++++

The FTP-Masters team is very small and is filled with folks who have been in
their position for a very long time. This leads to burn-out, which is readily
visible within the team.

Some Trainees have commented that this observed burn-out appears to contribute
to the slowness at which new members are processed.

This is a non-technical problem that Sean W. has been working with some success.

Impact
------

The slow processing time has been a known problem within the Debian community
for many years. This frustrates package maintainers by making it difficult to
get large / frequently-changing projects into the archive or to keep them there.

The large backlog and small team cause frequent long-term burn-out, which leads
to a negative feedback loop. The tools and demand of the team, in addition to the
added burden of training new members exasperates this problem.

The lack of a unified list of checks means we need to learn the list as we go,
from reading other reviews, and from being corrected. This means many new
trainee comments will be incorrect and should not be shared with the uploader,
which in turn contributes to an observed lack of transparency.

Potential Solutions
-------------------

Disband FTP-Masters
+++++++++++++++++++

This pops up from time to time and is frequently shot down as the necessity for
the team has been well-established and repeatedly proven. Debian has repeatedly
remained steadfast with it's commitment toward the DFSG and license compliance;
this team remains the primary mechanism in place to ensure compliance to this
commitment.

Peer Review you say? ... The FTP-Masters team catches a very high volume of
mistakes. It is extremely unlikely that any such system would prove effective.

REJECT List
+++++++++++

Checklists are absolutely fantastic tools that have been proven to be extremely
effective at eliminating mistakes. In fact, they have become a legal requirement
in many places because of their effectiveness.

For example: https://www.who.int/patientsafety/safesurgery/checklist/

Constructing a comprehensive list would aid new Trainees, provide the wider
community a thorough list of what the team looks for, and help prevent mistakes.

Web Reviews
+++++++++++

The `WEB_REVIEWS.rst`_ document describes a solution proposal meant to address
as many of the identified problems as possible by making use of the flexibility
offered by web browsers.

This includes features such as a stateful workspace, a comprehensive checklist,
review automation, many extra (automated) checks, and much more.

Note: The described project would automate many currently-manual checks, which
would help improve accuracy and consistency of reviews by changing the task of
the reviewer to that of verification rather than raw discovery.

CLI Tools
+++++++++

The ``Web Reviews`` proposal includes descriptions of libraries that should
eventually become Debian packages. These libraries will be designed to work with
the website as well as console-based applications.

These tools would be able to be used by the wider community as a way of
proof-reading their own work prior to upload.

dream::

    package-review --will-my-package-be-accepted foo.changes
    Checking package foo.changes. This will take a long time.
    Issues Identified:
    - Missing copyright for src/fuzzywuzzy
    - IETF document found
    - Source takes control of binaries from barpants
    CONCLUSION: REJECT LIKELY (100%)


.. _`WEB_REVIEWS.rst`: ./WEB_REVIEWS.rst
