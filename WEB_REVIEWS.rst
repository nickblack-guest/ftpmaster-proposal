Web Reviews
===========

The project-based solution put forward in this document describes a web-based
tool as a proposal to resolve issues identified in `README.rst`_.

Old Process
-----------

Some reviewers may prefer to stick with the old tools. It is important to note
that this proposed project *only* works as an extension of the current review
tools and *only* if ``d/copyright`` is machine-readable. The existing process
will remain available for all reviews.

Trainees would still need to learn these tools as well. However, prior
experience with the web-based interface should be able to lower the learning
curve.

Front-End Design
----------------

One of the primary concerns within the FTP-Master Web Review project is
usability. The design should be intuitive, easy to work with, etc.

Base Interface
++++++++++++++

The basic interface should include a very minimal layout with as much space as
possible reserved for review workspace as possible.

Expected design elements:

- Simple / Fast

  + Easy navigation, cacheable content, pre-built/static content
  + Pre-gzip static data

- Customizable
- Button to save layout
- Auto-save state
- Helpful (provide useful information)
- Minimalist design
- Debian SSO Authentication (DAK-based ACL)

**Tabs:**

The workspace area will include "tabs" that act as virtual workspaces which can
be renamed, re-arranged, added, and removed.

**Panels:**

Panels will be optionally added/removed per tab.

Available Panels:

- DAK Actions

  + Non-review actions to perform against a package.

- DAK Notes

  + Show the official list of DAK-managed notes

- DAK Templates

  + Templates used to provide easy and friendly responses

- Check Results

  + Display results of all the automated checks

- Copyright Info

  + Easily-read version of information provided in d/copyright
  + Ignore actual license text (if it looks correct)

- File List

  + List of files
  + Header w/ discovered copyright/license info

- Master Checklist

  + Unified collective checklist

- Personal Notes

  + Not shared
  + Stored per-user, not per-package

**Menus:**

A menu will exist that includes:

- Title & Link to home
- List of packages to review

  + A color to indicate review status
  + Sorted by upload time

.. image:: https://salsa.debian.org/mtecknology/ftpmaster-proposal/raw/master/_img/menu.png

**Default Display:**

A default display will be provided to new reviewers which includes all components
required for a simple, straight-forward, and effective reviews whether Trainee
or Master.

Three tabs:

- Notes

  + Master Checklist
  + Dak Notes
  + Dak Templates
  + Personal Notes
  + [note: this shows the "Panels" expansion]

.. image:: https://salsa.debian.org/mtecknology/ftpmaster-proposal/raw/master/_img/notes.png

- Review

  + Master Checklist
  + Copyright Info
  + File List
  + Check Results

.. image:: https://salsa.debian.org/mtecknology/ftpmaster-proposal/raw/master/_img/review.png

- Admin

  + Master Checklist
  + DAK Actions
  + DAK Notes
  + DAK Notes

.. image:: https://salsa.debian.org/mtecknology/ftpmaster-proposal/raw/master/_img/admin.png

Invocation
----------

Processing will be kicked off when dak processes uploaded packages and
determines that they should be held in the ``NEW`` queue. This is noted by the
``pkg_ver.chanegs is NEW`` email.

Collected information will be compiled into json files. This will help ensure
the application remains secure and fast. This json data will be stored next to
the unpacked source.

Automated Testing
-----------------

A major component of this project is automated testing. Tools exist to automate
reviewing source. Rather than continuing many manual processes which include an
objectionably significant learning curve, these tools could be used to gather
initial analysis. Rather than carefully keeping track of information, this
information shall be provided in the front-end interface. This allows the
reviewer to focus on automation accuracy and completeness.

At some point, it may become worthwhile to use the automated tests to
automatically prod and/or reject new packages.

Note: Use http://mentors.debian.net/ auto-check display as an example.

Planned tests include...

d/copyright
+++++++++++

In order for most of this process to work, a machine-readable ``d/copyright``
will be required. A check will exist to ensure this is possible.

License / Copyright
+++++++++++++++++++

The license check test will attempt to read ``d/copyright`` and match it against
source file license/copyright information and provide information about issues.

**ClearlyDefined**:

ClearlyDefined is a (currently under-utilized) project that scans source code to
identify license/copyright information. This tool provides an API that can be
used (with permission) as a backend to these checks.

This tool has already processed nearly every package in the Debian archive. The
ClearlyDefined team would even welcome dak automatically adding new sources when
new packages are added. (disclaimer: dak will /not/ distribute source; link only)

The "CD" team has also made it clear they consider any incorrectly identified
license/copyright a very serious problem.

License Compatibility
+++++++++++++++++++++

Although it's rare, some packages are uploaded containing sources with
incompatible licenses. This check simply cross-references a compatibility chart
and flags potential problems.

The Checklist
-------------

A non-customizable checklist will exist that covers everything an FTP Master
should look for during reviews. This checklist doubles as a way to provide a
clear and complete list of reasons that could cause a REJECT.

Miscellaneous
-------------

- The front page will show a (cached) status page, similar to `new.html`_
- Logged in users will see a list of pending packages with syntax identifying
  current state (pending review, scanned with/without errors, etc.)
- Logged in users will have access to a list of processed packages
- Previous reviews will be maintained for a configurable length of time

Future Thoughts
---------------

- After an extended period of testing, the proposed automated tests could be
  used to automatically mark a package for re-review via ``NEW`` if too many
  problems are identified.

- If implementation successfully improves review times, it could be incentive
  for others to ensure ``d/copyright`` is machine-readable and accurate.

.. _`README.rst`: ./README.rst
.. _`new.html`: https://ftp-master.debian.org/new.html
